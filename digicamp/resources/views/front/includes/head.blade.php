<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="format-detection" content="telephone=no" />
<meta http-equiv="Content-Language" content="id">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
<title>{{$meta['title']}} - {{$web_title}}</title>

<meta content="{{$meta['keywords']}}" name="keywords" />
<meta content="{{$meta['description']}}" name="description" />
<meta name="geo.placename" content="Indonesia">
<meta name="geo.country" content="ID">
<meta name="language" content="Indonesian">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta name="root_url" content="{{url($root_path)}}/" />

<!-- Core CSS -->
<link rel ="stylesheet" href="{{asset('components/plugins/bootstrap/css/bootstrap.min.css')}}">
<link rel ="stylesheet" href="{{asset('components/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<link rel ="stylesheet" href="{{asset('components/front/css/layout.min.css')}}">
<link rel ="stylesheet" href="{{asset('components/plugins/swiper-slider/swiper.min.css')}}">
<link rel ="stylesheet" href="{{asset('components/front/css/themes/default.min.css')}}">
<link rel ="stylesheet" href="{{asset('components/front/css/custom.css')}}">
<!-- <link rel ="stylesheet" href="{{asset('components/front/css/custom_style.css')}}"> -->
