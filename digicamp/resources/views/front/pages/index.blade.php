@extends($view_path.'.layouts.master')

@section('content')

<div class="container">
	    <div class="row search-box">
	    	<div class="col-md-12 col-sm-12">
	    	 	<form action="cek-alamat" method="GET">
		  		{{ csrf_field() }}
	            	<div class="input-group">
	                	<input type="text" placeholder="Search" name="search" class="form-control">
	                	<span class="input-group-btn">
	                    	<button class="btn" type="submit">Submit</button>
	                	</span>
	            	</div>
	        	</form>
           	</div>
	    </div>

	    <div class="row">
	      	<div class="col-md-12 col-sm-12">
	            <div class="portlet light bordered">
	                <div class="portlet-title">
	                    <div class="caption">
	                        <i class="icon-share font-blue"></i>
	                        <span class="caption-subject font-blue bold uppercase">Provinsi</span>
	                    </div>
	                    <div class="actions">
	                    </div>
	                </div>
	                <div class="portlet-body">
	                    <div class="scroller" style="" data-always-visible="1" data-rail-visible="0">
	                        <div class="table-responsive">
	                            <table class="table table-bordered">
	                                <!-- thead>
	                                    <tr>
	                                        <th> # </th>
	                                        <th> Name </th>
	                                        <th> Total Order Success</th>
	                                    </tr>
	                                </thead> -->
	                                <tbody>
	                                	@foreach($provinsi as $key => $val)
	                                		@php
	                                			$slug 	= str_replace(" ","-",strtolower($val->name));
	                                		@endphp
	                                		@if(($key)%3 == 0 )
	                                			<tr>
	                                		@endif
	                                			<td><a href="daerah/{{$slug}}">{{$val->name}}</a></td>
	                                	@endforeach
	                                </tbody>
	                            </table>
	                        </div>
	                    </div>
	                </div>


	            </div>
	        </div>
	    </div>
</div>

<hr/>

<div class="clear"></div>

@push('script')
	
@endpush
@endsection