@extends($view_path.'.layouts.master')
@section('content')
<style>

</style>

@push('styles')
<style>

</style>

<form role="form" method="post" action="{{url($path)}}/{{$data->id}}" enctype="multipart/form-data">
  {{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">        
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'name','label' => 'Subcategory Product Name','value' => (old('category_product_name') ? old('category_product_name') : $data->subcategory_product_name),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'category_product_name'])!!}

           <div class="col-md-6">
              <div class="form-group" style=''>
                    <label for="tag">Category Product</label>
                    <select class="select2" name="category_product">
                      <option value="0">-- Please Select Category Product --</option>
                      @foreach($category_product as $u)
                          <option value="{{$u->id}}" {{old('category_product') ? (in_array($u,[old('category_product')]) ? 'selected' : '') : ($data->category_product_id ? ($u->id == $data->category_product_id ? 'selected' : ''): '')}}>{{$u->category_product_name}}</option>
                      @endforeach
                    </select>
              </div>
          </div>
      </div>

      <div class="row">
          <div class="col-md-6">
              <div class="form-group" style=''>
                    <label for="tag">Parent</label>
                    <select class="select2" name="parent">
                      <option value="0">-- Please Select Parent --</option>
                      @foreach($parent as $u)
                          <option value="{{$u->id}}" {{old('parent') ? (in_array($u,[old('parent')]) ? 'selected' : '') : ($data->parent_id ? ($u->id == $data->parent_id ? 'selected' : ''): '')}}>{{$u->subcategory_product_name}}</option>
                      @endforeach
                    </select>
              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group" style=''>
                    <label for="tag">Operator</label>
                    <select class="select2" name="operator">
                      <!-- <option value="0">-- Please Select Parent --</option> -->
                      @foreach($operator as $u)
                          <option value="{{$u->id}}" {{old('operator') ? (in_array($u,[old('operator')]) ? 'selected' : '') : ($data->operator_id ? ($u->id == $data->operator_id ? 'selected' : ''): '')}}>{{$u->operator_name}}</option>
                      @endforeach
                    </select>
              </div>
          </div>

          {!!view($view_path.'.builder.textarea',['name' => 'description','label' => 'Description','value' => (old('description') ? old('description') : $data->description),'attribute' => 'autofocus','form_class' => 'col-md-6', 'class' => 'description'])!!}

         <div class="col-md-12 actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
        </div>
    </div>
</form>

@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
        
  </script>
@endpush
@endsection
