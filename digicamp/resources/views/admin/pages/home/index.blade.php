@extends($view_path.'.layouts.master')
@section('content')
<form role="form" method="post" action="{{url($path)}}/update" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green">
          <i class="icon-layers font-green title-icon"></i>
          <span class="caption-subject bold uppercase"> {{$title}}</span>
        </div>

        <div class="actions">
        <div class="actions">
          <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
        </div>
      </div>
    </div>
    <datalist></datalist>
      @include('admin.includes.errors')
      <div class="tabbable-line">
          <div class="row">
            <div class="form-group col-md-12 form-group-promo">
              <label for="tag" class="sub-title">Slideshow</label>
            </div>
            @foreach($slideshow as $s)
            <div class="form-group form-md-line-input col-md-12">
                <label>{{$s->name}}</label><br>
                <label class="btn green input-file-label-logo">
                  <input type="file" class="form-control col-md-12 single-image" name="slideshow_{{$s->id}}"> Pilih File
                </label>
                    <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="slideshow_{{$s->id}}">Hapus</button>
                  <input type="hidden" name="remove-single-image-slideshow_{{$s->id}}" value="n">
                  <br>
                <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>

                <div class="form-group single-image-slideshow_{{$s->id}} col-md-12">
                  <img src="{{$s->image != null ? asset($image_path3.'/'.$s->image) : asset($image_path2).'/none.png'}}" class="img-responsive thumbnail single-image-thumbnail">
                </div>

                <div class="form-group col-md-12">
                  <input type="text" class="editor" name="slideshow_description_{{$s->id}}" value="{{$s->description}}" />
                </div>
            </div>
            @endforeach
            </hr>

            <div class="form-group col-md-12 form-group-promo">
              <label for="tag" class="sub-title">Banner</label>
            </div>
            @foreach($banner as $b)
            <div class="form-group form-md-line-input col-md-12">
                <label>{{$b->page}}</label><br>
                <label class="btn green input-file-label-logo">
                  <input type="file" class="form-control col-md-12 single-image" name="banner_{{$b->id}}"> Pilih File
                </label>
                    <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="banner_{{$b->id}}">Hapus</button>
                  <input type="hidden" name="remove-single-image-banner_{{$b->id}}" value="n">
                  <br>
                <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>

                <div class="form-group single-image-banner_{{$b->id}} col-md-12">
                  <img src="{{$b->image != null ? asset($image_path4.'/'.$b->image) : asset($image_path2).'/none.png'}}" class="img-responsive thumbnail single-image-thumbnail">
                </div>

               <div class="form-group col-md-12">
                  <input type="text" class="col-md-12 editor" name="banner_description_{{$b->id}}" value="{{$b->description}}" />
                </div>
            </div>
            @endforeach
            <hr>

            <div class="form-group col-md-12 form-group-promo">
              <label for="tag" class="sub-title">Why Us</label>
            </div>
                {!!view($view_path.'.builder.text',['type' => 'text','name' => 'alasan_1_name','label' => 'Why Us 1 Name','value' => (old('alasan_1') ? old('alasan_1') : $home->alasan_1_name),'attribute' => 'required','form_class' => 'col-md-12'])!!}

                {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'alasan_1','label' => 'Why Us 1 Description','value' => (old('alasan_1') ? old('alasan_1') : $home->alasan_1),'attribute' => 'required','form_class' => 'col-md-12'])!!}

                {!!view($view_path.'.builder.text',['type' => 'text','name' => 'alasan_2_name','label' => 'Why Us 2 Name','value' => (old('alasan_2') ? old('alasan_2') : $home->alasan_2_name),'attribute' => 'required','form_class' => 'col-md-12'])!!}

                 {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'alasan_2','label' => 'Why Us 2 Description','value' => (old('alasan_1') ? old('alasan_1') : $home->alasan_2),'attribute' => 'required','form_class' => 'col-md-12'])!!}

                {!!view($view_path.'.builder.text',['type' => 'text','name' => 'alasan_3_name','label' => 'Why Us 3 Name','value' => (old('alasan_3') ? old('alasan_3') : $home->alasan_3_name),'attribute' => 'required','form_class' => 'col-md-12'])!!}

                 {!!view($view_path.'.builder.textarea',['type' => 'text','name' => 'alasan_3','label' => 'Why Us 3 Description','value' => (old('alasan_1') ? old('alasan_1') : $home->alasan_3),'attribute' => 'required','form_class' => 'col-md-12'])!!}
            <hr>

            <div class="form-group col-md-12 form-group-promo">
              <label for="tag" class="sub-title">Footer</label>
            </div>
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'operational_hour','label' => 'Operational Hour','value' => (old('operational_hour') ? old('operational_hour') : $home->operational_hour),'attribute' => 'required','form_class' => 'col-md-12', 'class' => 'editor'])!!}
          </div>
            <hr/>
          <div class="row">
            <div class="col-md-12 actions">
              {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
            </div>
          </div>
          
      </div>
    </div>
  </div>
</form>
@push('custom_scripts')
  @if ($role->view == 'n')
    <script>
      $(document).ready(function(){
        // $('input,select,textarea').prop('disabled',true);
      });
    </script>
  @endif
@endpush
@endsection
