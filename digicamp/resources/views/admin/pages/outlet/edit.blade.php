@extends($view_path.'.layouts.master')
@section('content')

<form role="form" method="post" action="{{url($path)}}/{{$data->id}}" enctype="multipart/form-data">
	{{ method_field('PUT') }}
	<div class="portlet light bordered">
	    <div class="portlet-title">
	      <div class="caption font-green">
	        <i class="icon-layers font-green title-icon"></i>
	        <span class="caption-subject bold uppercase"> {{$title}}</span>
	      </div>
	      <div class="actions">
	        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
	      </div>
	    </div>
	    <div class="portlet-body form">
	      	@include('admin.includes.errors')
            <div class="row">
  				{!!view($view_path.'.builder.text',['type' => 'text','name' => 'name','label' => 'Outlet Name','value' => (old('name') ? old('name') : $data->outlet_name),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => '', 'required'=>'y'])!!}

	            {!!view($view_path.'.builder.textarea',['name' => 'address','label' => 'Address','value' => (old('address') ? old('address') : $data->address),'class' => '','form_class' => 'col-md-6','attribute' => 'required', 'required'=>'y'])!!}

	            <div class="col-md-6">
		            <div class="form-group" style=''>
		                  <label for="tag">Province</label>
		                  <select class="select2" name="province">
		                  	<option value="">Select Province</option>
		                  	@foreach($province as $s)
		                  		<option value="{{$s->id}}-{{$s->name}}" {{old('province') ? (in_array($s->name,[old('province')]) ? 'selected' : '') : ($s->id == $data->province_id ? 'selected' : '')}}>{{$s->name}}</option>
		                  	@endforeach
		                  </select>
	                </div>
	            </div>

	            <div class="col-md-6">
		            <div class="form-group" style=''>
		                  <label for="tag">Outlet Parent<span class="required" aria-required="true">*</span></label>
		                  <select class="select2" name="outlet_pusat">
		                  	<option value="0">-- Please Select Outlet Parent --</option>
		                  	@foreach($outlet_pusat as $op)
		                  		<option value="{{$op->id}}" {{old('outlet_pusat') ? (in_array($op->id,[old('outlet_pusat')]) ? 'selected' : '') : ($op->id == $data->outlet_pusat ? 'selected' : '')}}>{{$op->outlet_name}}</option>
		                  	@endforeach
		                  </select>
	                </div>
	            </div>

	            <div class="form-group form-md-line-input col-md-12">
	             	<label>Image</label><br>
		          	<label class="btn green input-file-label-image">
		            	<input type="file" class="form-control col-md-12 single-image" name="image"> Pilih File
		          	</label>
		             
		             <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="image">Hapus</button>
		            <input type="hidden" name="remove-single-image-image" value="n">
		            <br>
		          	<small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>

			        <div class="form-group single-image-image col-md-12">
			            <img src="{{asset($image_path.'/'.$data->images)}}" onerror="this.src='{{ asset($image_path2.'/'.'none.png') }}';" class="img-responsive thumbnail single-image-thumbnail">
			        </div>
		        </div>

		        <div class="col-md-12 actions">
		        	{!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}	
		       	</div>
			</div>
		</div>
	</div>
</form>
@push('custom_scripts')
	<script>
		$(document).ready(function(){
			var password = $(".password");

			$('#generate-password').on('click', function(e){
	        	var randomstring = Math.random().toString(36).slice(-6);
		        password.val(randomstring);
		    });

		    $('#show-password').on('click', function(e){
		        if(password.attr("type") == "password"){
		            password.attr("type", "text");
		            $("#show-password").addClass("text-primary");
		            $("#show-password").removeClass("text-default");
		        }
		        else{
		            password.attr("type", "password");
		            $("#show-password").addClass("text-default");
		            $("#show-password").removeClass("text-primary");
		        }
		    });
		});
	</script>
@endpush
@endsection