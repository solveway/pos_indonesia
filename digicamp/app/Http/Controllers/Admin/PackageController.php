<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Config;
use digipos\models\Product;
use digipos\models\Package;
use digipos\models\Category_Product;
use digipos\models\Subcategory_product;
use digipos\models\Operator;
use digipos\models\Package_product;

use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use digipos\Libraries\Email;
use Carbon\Carbon;
use File;

class PackageController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "Package";
		$this->data['title']	= $this->title;
		$this->root_link 		= "package";
		$this->model 			= new Package;
		$this->package_product  = new Package_product;

		$this->bulk_action			= false;
		$this->bulk_action_data 	= [2];
		$this->image_path 			= 'components/both/images/package/';
		$this->data['image_path'] 	= $this->image_path;	
		$this->image_path2 			= 'components/both/images/web/';
		$this->data['image_path2'] 	= $this->image_path2;
		$this->unit 				= ['Pcs','Psi'];

		$this->meta_title = Config::where('name', 'web_title')->first();
        $this->meta_description = Config::where('name', 'web_description')->first();
        $this->meta_keyword = Config::where('name', 'web_keywords')->first();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$this->field = [
			[
				'name' 		=> 'package_name',
				'label' 	=> 'Package Name',
				'search' 	=> 'text',
				'sorting' 	=> 'y'
			],
			[
				'name' 		=> 'barcode',
				'label' 	=> 'Package Name',
				'search' 	=> 'text',
				'sorting' 	=> 'y'
			],
			[
				'name' 		=> 'description',
				'label' 	=> 'Description',
				'search' 	=> 'text',
				'sorting' 	=> 'y'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'type' 		=> 'check',
				'data' 		=> ['y' => 'Active','n' => 'Not Active'],
				'tab' 		=> 'general'
			]
		];

		return $this->build('index');
	}

	public function create(){
		$this->data['title'] 				= "Create Package";
		$this->data['product_exist'] 		= Package_product::get();
		// dd($this->data['product_exist']);
		$this->data['product']  			= Product::join('subcategory_product', 'subcategory_product.id', 'product.subcategory_product_id')->Join('operator', 'operator.id', 'subcategory_product.operator_id')->select('product.*','operator.operator_name as operator_name')->where('product.status', 'y')->where('product.subcategory_product_id', '!=', 0)->whereNotIn('product.id', function($q){$q->select('product_id')->from('package_product')->where('status', 'y');})->get();
		
		$this->data['package']				= $this->model->where([['status', 'y'],['package_id', 0]])->get();
		// dd($this->data['service_category']);

		// dd($this->data['package']);
		return $this->render_view('pages.package.create');
	}

	public function store(Request $request){
		$this->validate($request,[
			'package_name' 			=> 'required',
		]);

		if(!is_numeric($request->qty)){
			Alert::fail('Qty must be number');
			return redirect()->to($this->data['path'].'/create')->withInput($request->input());
		}

		if(!is_numeric($request->percent)){
			Alert::fail('Percent must be number');
			return redirect()->to($this->data['path'].'/create')->withInput($request->input());
		}

		// dd($request->product);
		$level = 0;
		// if($request->parent != 0){
		// 	$data = $this->model->get();

		// 	if($data){
		// 		$level = 1;
		// 		$package_id = $request->package_id;
		// 		for($i=0; $i<count($data); $i++){
		// 			if($data[$i]['id'] == $package_id){
		// 				if($level == 0){
		// 					$level = 1;
		// 				}

		// 				if($data[$i]['level'] != 0){
		// 					$data[$i]['level'] += 1;
		// 					$i = 0;
		// 					$package_id = $data[$i]['package_id'];
		// 				} 
		// 			}
		// 		}
		// 	}
		// }

		if($request->parent != 0){
        	$data = $this->model->where('id', $request->parent)->first();
        	
        	if(count($data) > 0){
        		$level = $data->level + 1;
        	}
        }

		$this->model->package_name			= $request->package_name;
		$this->model->package_id			= $request->parent;
		$this->model->description			= $request->description;
		$this->model->percent				= $request->percent;
		$this->model->barcode				= $request->barcode;
		$this->model->no_box				= $request->no_box;
		$this->model->qty					= $request->qty;
		$this->model->valid_date			= date_format(date_create($request->valid_date),'d-m-Y');
		$this->model->level 				= $level;
		$this->model->parent_status 		= 'n';
		$this->model->status 				= 'y';
		$this->model->upd_by 				= auth()->guard($this->guard)->user()->id;
		
		if ($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}

		// dd($this->model);
		$this->model->save();
		// dd($request->product);
		$product = $request->product;
		if($product){
			foreach ($product as $key => $p_id) {
				$temp_product[]			= [
					'package_id' => $this->model->id,
					'product_id' => $p_id,
					'status' 	 => 'y',
					'upd_by'	 => auth()->guard($this->guard)->user()->id,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now()
				];
			}

			if(count($temp_product) > 0){
				// dd($temp_product);
				Package_product::insert($temp_product);
			}
		}
		
		// $product_id							= $request->product_id;
		// $temp_product						= [];
		// if($product_id){
		// 	$qty_default						= $request->qty_default;
		// 	foreach ($product_id as $key => $p_id) {
		// 		$qtyDefault		= $qty_default[$key];

		// 		$temp_product[]			= [
		// 			'service_product_id'	=> $this->model->id,
		// 			'product_id'			=> $p_id,
		// 			'qty_default'			=> $qtyDefault,
		// 			'upd_by'				=> auth()->guard($this->guard)->user()->id,
		// 			'created_at'			=> Carbon::now(),
		// 			'updated_at'			=> Carbon::now()
		// 		];
		// 	}

		// 	if(count($temp_product) > 0){
		// 		// dd($temp_product);
		// 		Service_product_dt::insert($temp_product);
		// 	}
		// }

		Alert::success('Successfully add new Package');
		return redirect()->to($this->data['path']);
	}

	public function edit($id){
		$this->model 						= $this->model->find($id);
		$this->data['title'] 				= "Edit Package ".$this->model->package_name." - ".$this->model->barcode;
		$package_product 					= Package_product::where([['package_id', $id],['status', 'y']])->pluck('product_id')->toArray();
		// dd($package_product);
		$package_product 					= $this->formatArrayToTitikKoma($package_product);
		$this->data['package_product']		= $package_product;
		// dd($product_exist);
		$this->data['product']  			= Product::join('subcategory_product', 'subcategory_product.id', 'product.subcategory_product_id')->Join('operator', 'operator.id', 'subcategory_product.operator_id')->select('product.*', 'operator.operator_name')->where('product.status', 'y')->where('product.subcategory_product_id', '!=', 0)->whereNotIn('product.id', function($q)use($id){$q->select('product_id')->from('package_product')->where([['status', 'y'],['package_id','!=', $id]]);})->get();
		// dd($this->data['product']);
		$this->data['package']				= $this->model->where([['status', 'y'],['package_id', 0]])->get();
		$this->data['data'] 				= $this->model;
		return $this->render_view('pages.package.edit');
	}

	public function update(Request $request, $id){
		
		$this->validate($request,[
			'package_name' 			=> 'required',
		]);

		if(!is_numeric($request->qty)){
			Alert::fail('Qty must be number');
			return redirect()->to($this->data['path'].'/create')->withInput($request->input());
		}

		if(!is_numeric($request->percent)){
			Alert::fail('Percent must be number');
			return redirect()->to($this->data['path'].'/create')->withInput($request->input());
		}

		// dd($request->product);
		$level = 0;
		// if($request->parent != 0){
		// 	$data = $this->model->get();

		// 	if($data){
		// 		$level = 1;
		// 		$package_id = $request->package_id;
		// 		for($i=0; $i<count($data); $i++){
		// 			if($data[$i]['id'] == $package_id){
		// 				if($level == 0){
		// 					$level = 1;
		// 				}

		// 				if($data[$i]['level'] != 0){
		// 					$data[$i]['level'] += 1;
		// 					$i = 0;
		// 					$package_id = $data[$i]['package_id'];
		// 				} 
		// 			}
		// 		}
		// 	}
		// }

		$this->model 						= $this->model->find($id);

		if($request->parent != 0){
			//get package which have id request parent
        	$data = Package::where('id', $request->parent)->first();
        	
        	if(count($data) == 1){
        		$level = $data->level + 1;
        	}

        	//if subcategory has child
        	// $data2 = $this->model->where([['id', $id]])->get();
        	// dd($subcategoryProduct2);

        	// $child = $this->model->where('parent_id', $id)->pluck('id')->toArray();
        	//if subcategory has child

        }else{ 
        	// check if has not parent then change level to 0
        	// $subcategoryProduct = $this->model->where('parent_id', $request->parent)->first();
        	// $subcategory->level = 0;

        	// $subcategoryProduct2 = $this->model->where('id', $subcategory->id)->get();
        	// // if has child then minus child subcategory with 1
        	// if(count($subcategoryProduct2)){
        	// 	foreach($subcategoryProduct2 as $sb2){
        	// 		$sb2->level -= 1;
        	// 		$sb2->save();
        	// 	}
        	// }
        }

        if($this->model->parent_status == 'y'){
    		$curr_id = $id;
    		$curr_level = $level;
    		//get full table

    		$data2 		= Package::where([['id', '!=', $id]])->orderBy('level', 'asc')->get();
    		for($i=0; $i<count($data2); $i++){
    			if($data2[$i]['parent_id'] == $curr_id){
    				$curr_level 	+= 1;
    				$data3 			= Package::find($data2[$i]['id']); 			
    				$data3->level 	= $curr_level;
					$data3->save();
					$curr_id 		= $data3->id;

					if($data2['$i']['parent_status'] == 'y'){
						$i = 0;
					}else{
						break;
					}
    			}
    		}
    	}

		
		$this->model->package_name			= $request->package_name;
		$this->model->package_id			= $request->parent;
		$this->model->description			= $request->description;
		$this->model->percent				= $request->percent;
		$this->model->barcode				= $request->barcode;
		$this->model->no_box				= $request->no_box;
		$this->model->qty					= $request->qty;
		$this->model->valid_date			= date_format(date_create($request->valid_date),'d-m-Y');
		$this->model->level 				= $level;
		$this->model->status 				= 'y';
		$this->model->upd_by 				= auth()->guard($this->guard)->user()->id;
		
		if ($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}

		// dd($this->model);
		$this->model->save();
		// dd($request->product);
		$product 				= $request->product;
		$package_product 		= Package_product::where('package_id', $id)->get();
		if(count($package_product) > 0){
			Package_product::where('package_id', $id)->update(['status' => 'n']);
		}
		if($product){
			foreach($product as $key => $p_id) {
				$package_product 		= Package_product::where([['package_id', $id],['product_id', $p_id]])->first();
				$temp_product 			= [];
				// dd($package_product);
				if(count($package_product) == 1){
					Package_product::where([['package_id', $id],['product_id', $p_id]])->update(['status' => 'y']);
				}else{
					$temp_product[]			= [
						'package_id' => $this->model->id,
						'product_id' => $p_id,
						'status' 	 => 'y',
						'upd_by'	 => auth()->guard($this->guard)->user()->id,
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					];
				}
			}

			if(count($temp_product) > 0){
				// dd($temp_product);
				Package_product::insert($temp_product);
			}
		}
		
		// $product_id							= $request->product_id;
		// $temp_product						= [];
		// if($product_id){
		// 	$qty_default						= $request->qty_default;
		// 	foreach ($product_id as $key => $p_id) {
		// 		$qtyDefault		= $qty_default[$key];

		// 		$temp_product[]			= [
		// 			'service_product_id'	=> $this->model->id,
		// 			'product_id'			=> $p_id,
		// 			'qty_default'			=> $qtyDefault,
		// 			'upd_by'				=> auth()->guard($this->guard)->user()->id,
		// 			'created_at'			=> Carbon::now(),
		// 			'updated_at'			=> Carbon::now()
		// 		];
		// 	}

		// 	if(count($temp_product) > 0){
		// 		// dd($temp_product);
		// 		Service_product_dt::insert($temp_product);
		// 	}
		// }

		Alert::success('Successfully add new Package');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->model 						= $this->model->find($id);
		$this->data['title'] 				= "Edit Service ".$this->model->product_name;
		$this->data['product']  			= Product::where('status', 'y')->get();
		$this->data['service_category']		= Category_service::get();
		$this->data['data']  				= $this->model;
		$this->data['data2']  				= Service_product_dt::join('product','product.id','service_product_dt.product_id')->select('service_product_dt.*', 'product.product_name as product_name')->where('service_product_id', $id)->get();

		return $this->render_view('pages.service_product.view');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		// dd('bulkupda');
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function export(){
		return $this->build_export_cus();
	}
}
