<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Config;
use digipos\models\Product;
use digipos\models\Outlet;
use digipos\models\Product_package_adjustment;
use digipos\models\Province;
use digipos\models\Package;
use digipos\models\Package_product;

use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use digipos\Libraries\Email;
use Carbon\Carbon;
use File;

class ProductAdjustmentController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "Adjustment";
		$this->data['title']	= $this->title;
		$this->root_link 		= "adjustment";
		$this->serial_product 	= Product::join('package_product', 'package_product.id', 'product.id')->join('package', 'package.id', 'package_product.package_id')->select('product.*', 'product.description as product_description','package.package_name', 'package.barcode as package_barcode', 'package.description as package_description', 'package_product.product_id as package_product_product_id', 'package_product.package_id as package_product_package_id', 'package_product.status as package_product_status');
		$this->outlet 			= new Outlet;
		$this->model 			= $this->outlet;

		$this->bulk_action			= false;
		// $this->bulk_action_data 	= [3];
		$this->image_path 			= 'components/both/images/product/';
		$this->data['image_path'] 	= $this->image_path;
		$this->image_path2 			= 'components/both/images/web/';
		$this->data['image_path2'] 	= $this->image_path2;
		$this->unit 				= ['Pcs','Psi'];

		$this->meta_title = Config::where('name', 'web_title')->first();
        $this->meta_description = Config::where('name', 'web_description')->first();
        $this->meta_keyword = Config::where('name', 'web_keywords')->first();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		// $desc_filter = Order_status::select('desc')->whereIn('id', [1,2,3,4,5,6,11])->get();

		// foreach($desc_filter as $dc){
		// 	$dc_filter[$dc->desc] = $dc->desc;
		// }

		$this->field = [
			// [
			// 	'name' => 'images',
			// 	'label' => 'Image',
			// 	'type' => 'image',
			// 	'file_opt' => ['path' => $this->image_path, 'custom_path_id' => 'y']
			// ],
			[
				'name' 		=> 'outlet_name',
				'label' 	=> 'Outlet Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			// [
			// 	'name' 		=> 'product_name',
			// 	'label' 	=> 'Product Name',
			// 	'sorting' 	=> 'y',
			// 	'search' 	=> 'text'
			// ],
			// [
			// 	'name' 		=> 'package_description',
			// 	'label' 	=> 'Package Description',
			// 	'sorting' 	=> 'n',
			// 	'search' 	=> 'text'
			// ],
			// [
			// 	'name' 		=> 'product_description',
			// 	'label' 	=> 'Product Description',
			// 	'sorting' 	=> 'n',
			// 	'search' 	=> 'text'
			// ],
			// [
			// 	'name' 		=> 'description',
			// 	'label' 	=> 'Description',
			// 	'sorting' 	=> 'n',
			// 	'search' 	=> 'text'
			// ],
			// [
			// 	'name' 		=> 'barcode',
			// 	'label' 	=> 'Barcode',
			// 	'sorting' 	=> 'y',
			// 	'search' 	=> 'text'
			// ],
			// [
			// 	'name' 		=> 'status',
			// 	'label' 	=> 'Status',
			// 	'sorting' 	=> 'y',
			// 	'search' 	=> 'text'
			// ],
			// [
			// 	'name'			=> 'updated_at',
			// 	'label'			=> 'Adjustment Datetime',
			// 	'belongto'		=> ['method' => 'product_adjustment', 'field' => 'updated_at'],
			// 	'sorting' 		=> 'y',
			// 	// 'search'		=> 'select',
			// 	// 'search_data' 	=> $st_order,
			// ]
		];

		// dd($this->model->get());
		$this->model = $this->model->join('province', 'province.id', 'outlet.province_id')->where('outlet.status', 'y')->select('outlet.*');
		// dd($this->model->get());
		return $this->build('index');
	}

	public function create(){
		
		// $this->data['title'] 			= "Create product";
		// $this->data['unit']  			= $this->unit;
		// // $this->data['province']		= Province::get();

		// return $this->render_view('pages.product.create');
	}

	public function store(Request $request){
		// $this->validate($request,[
		// 	'name' 		=> 'required|unique:product,product_name',
		// ]);

		// $this->model->product_name			= $request->name;
		// $this->model->description			= $request->description;
		// $this->model->unit					= $request->unit;
		// $this->model->price					= $this->decode_rupiah($request->price);
		// $this->model->status 				= 'y';
		// $this->model->upd_by 				= auth()->guard($this->guard)->user()->id;

		// if ($request->hasFile('image')){
  //       	// File::delete($path.$user->images);
		// 	$data = [
		// 				'name' => 'image',
		// 				'file_opt' => ['path' => $this->image_path.$this->model->id.'/']
		// 			];
		// 	$image = $this->build_image($data);
		// 	$this->model->images = $image;
		// }

		// // dd($this->model);
		// $this->model->save();

		// Alert::success('Successfully add new Outlet');
		// return redirect()->to($this->data['path']);
	}

	public function edit($id){
		$this->model 						= $this->model->find($id);
		$this->data['title'] 				= "Edit Product ".$this->model->product_name;
		
		$this->data['adjust']  				= Product_package_adjustment::join('outlet', 'outlet.id', 'product_package_adjustment.outlet_id')->join('product', 'product.id', 'product_package_adjustment.product_id')->join('package', 'package.id', 'product_package_adjustment.package_id')->select('product_package_adjustment.*', 'outlet.outlet_name', 'product.product_name', 'package.package_name', 'package.package_id')->where('outlet_id', $id)->orderBy('id', 'desc')->get();
		// dd($this->data['adjust']);
		// $this->data['buying_price_average'] = Product_adjustment::where('product_id', $id)->orderBy('product_adjustment.id', 'desc')->first();
		// dd($this->data['buying_price_average']);
		// dd($this->data['adjust']);
		// $this->data['serial_product']  		= Product_package_adjustment::join('outlet', 'outlet.id', 'product_package_adjustment.outlet_id')->join('product', 'product.id', 'product_package_adjustment.product_id')->join('package', 'package.id', 'product_package_adjustment.package_id')->join('product_type', 'product_type.id', 'product.product_type_id')->select('product_package_adjustment.*', 'outlet.outlet_name', 'product.product_name', '','package.package_name', 'package.package_id')->where([['outlet_id', $id],['']])->orderBy('id', 'desc')->get();

		//get product where outlet no this outlet, and id adjustemnt package or product in other outlet
		$alreadyInOtherOutlet 				= Product_package_adjustment::where('outlet_id', '!=', $id)->where('id', DB::raw("(select max(`id`) from te_product_package_adjustment ppa where outlet_id = te_product_package_adjustment.outlet_id)"))->pluck('id')->toArray(); 
		// dd($alreadyInOtherOutlet);

		//get product where not in package
		$this->data['serial_product']  		= Product::join('product_type', 'product_type.id', 'product.product_type_id')->whereNotIn('product.id', function($q)use($id){$q->select('product_id')->from('package_product')->where([['status', 'y']]);})->where([['product_type_name','Serial']])->get();

		//get package which has product
		$this->data['package_small']  				= Package::join('package_product', 'package_product.package_id', 'package.id')->where([['package.status', 'y'], ['package_product.status', 'y'], ['package.package_id', 0]])->select('package.*', 'package_product.product_id as package_product_product_id', 'package_product.package_id as package_product_package_id')->groupBy('package_product.package_id')->get();

		//get parent package which package has product
		$this->data['package_big']  			= Package::where([['status', 'y'],['package_id', '0']])->get();
		// dd($this->data['package']);
		$this->data['qty_product']  		= Product::join('product_type', 'product_type.id', 'product.product_type_id')->where([['product_type_name','Quantity']])->get();
		// $this->data['province']		= Province::get();

		//get package product which has this outlet id from product_package_adjustment
		$this->data['package_product_exist'] 	= ';1;10';
		$this->data['package_small_exist'] 		= ';10;';
		$this->data['package_big_exist'] 		= ';1;';
		$this->data['data']  					= $this->model;	

		// dd($this->data['package_small']);
		return $this->render_view('pages.product_adjustment.edit');
	}

	public function update(Request $request, $id){
		$this->validate($request,[
			'buying_price' 		=> 'required',
		]);
		
		if($request->buying_price == 'NaN'){
			Alert::fail('Buying Price must numeric !');
			return redirect()->to($this->data['path'].'/'.$id.'/edit')->withInput($request->input());
		}

		
		$this->model 					= $this->model->find($id);
		$buying_price					= $this->decode_rupiah($request->buying_price);

		$upd_by 						= auth()->guard($this->guard)->user()->id;

		$product_adjustment  			= Product_adjustment::where('id', DB::raw("(select max(`id`) from ni_product_adjustment npa where outlet_id = ni_product_adjustment.outlet_id)"))->where('product_adjustment.product_id', $id)->orderBy('id', 'desc')->groupBy('outlet_id')->get();
		// dd($product_adjustment);
		$adjust 						= $request->adjust;
		// dd($adjust);
		$data = [];

		$temp_arr = [];
		$global_stock_total_prev 				= 0;
		$global_stock_total_prev2 				= 0;
		$global_buying_price_average_prev 		= 0;
		$global_buying_price_average_prev2 		= 0;
		$flagStatus 							= 0;

		$outlet_stock_total 			= 0;
		$qty 							= 0;
		$global_stock_total				= 0;
		$outlet_buying_price_average 	= 0;
		$global_buying_price_average 	= 0;
		$buying_price2					= 0;
		$buying_price_prev				= 0;
		$flagFilledOutlet 				= 0;
		foreach ($this->outlet as $key => $dy) {
			if($adjust[$key] != ''){
				$flagFilledOutlet = 1;
				var_dump('$adjust[$key]: '.$adjust[$key]);
				if(count($product_adjustment) > 0){
					$flagExist = 0;
					foreach($product_adjustment as $key2 => $p){
						//get last product adjustment
						if($key2 == 0){
							if($global_stock_total_prev == 0){
								$global_stock_total_prev 			= $p->global_stock_total;
							}

							if($global_buying_price_average_prev == 0){
								$global_buying_price_average_prev	=  $p->global_buying_price_average;
							}

							if($global_stock_total_prev2 == 0){
								$global_stock_total_prev2			=  $p->global_stock_total;
							} 

							if($buying_price_prev == 0){
								$buying_price_prev 					= $p->outlet_buying_price;
							}
						}

						if($p->outlet_id == $dy->id && $p->product_id == $this->model->id){
							$flagExist = 1;

							$qty 							= $adjust[$key] - $p->outlet_stock_total;
							$outlet_stock_total				= $qty + $p->outlet_stock_total;
							if($global_stock_total_prev != 0){
								$global_stock_total				= $qty + $global_stock_total_prev;
							}else{
								$global_stock_total				= $qty + $global_stock_total;
							}
							

							if($qty > 0){
								var_dump('global_stock_total_prev: '.$global_stock_total_prev);
								var_dump('global_buying_price_average_prev: '.$global_buying_price_average_prev);
								if($global_stock_total != 0){
									$buying_price2 					= $buying_price;
									$global_buying_price_average	= (($global_stock_total_prev * 
									$global_buying_price_average_prev) + ($qty * $buying_price2))/ ($global_stock_total);
								}
							}else{
								$buying_price2 					= $global_buying_price_average_prev;
								if($global_stock_total != 0){
									$global_buying_price_average	= (($global_buying_price_average_prev * $global_stock_total_prev) + ($qty * $global_buying_price_average_prev)) /  $global_stock_total;
								}
							}
						}
					}

					//if this outlet and product didn't find in previous database
					if($flagExist == 0){
						$outlet_stock_total 			= $adjust[$key];
						$qty 							= $adjust[$key];
						$global_stock_total				= $adjust[$key] + $global_stock_total_prev;
						$buying_price2 					= $buying_price;
						var_dump('$global_stock_total_prev: '.$global_stock_total_prev);
						var_dump('$global_buying_price_average_prev: '.$global_buying_price_average_prev);
						$global_buying_price_average	= (($global_stock_total_prev * $global_buying_price_average_prev) + ($qty * $buying_price))/ ($global_stock_total);
					}
				}else{
					$outlet_stock_total 			= $adjust[$key];
					$qty 							= $adjust[$key];
					$global_stock_total				= $adjust[$key] + $global_stock_total_prev;
					$global_buying_price_average 	= $buying_price;
					$buying_price2 					= $buying_price;
				}
				var_dump('$qty: '.$qty);
				if($qty != 0){
					$temp_arr[]			= [
						'outlet_id'						=> $dy->id,
						'product_id'					=> $this->model->id,
						'qty'							=> $qty,
						'outlet_stock_total'			=> $outlet_stock_total,
						'global_stock_total'			=> $global_stock_total,
						'outlet_buying_price'			=> $buying_price2,
						'global_buying_price_average'	=> $global_buying_price_average,
						'status'						=> 'adjust',
						'upd_by'						=> $upd_by,
						'created_at'					=> Carbon::now(),
						'updated_at'					=> Carbon::now()

					];

					$stock = 0;
					$global_stock_total_prev 	 		= $global_stock_total;
					$global_buying_price_average_prev2 	= $global_buying_price_average;
					$global_buying_price_average_prev 	= $global_buying_price_average;
					$global_stock_total_prev2 			= $global_stock_total;
					$buying_price_prev 					= $buying_price2;
					var_dump('$global_stock_total_prev2_2: '.$global_stock_total_prev2.'<br>');
				}
			}
		}
		if($flagFilledOutlet == 0){
			Alert::fail('No outlet filled !');
			return redirect()->to($this->data['path'].'/'.$id.'/edit')->withInput($request->input());
		} 
		// dd($temp_arr);
		if(count($temp_arr) > 0){
			Product_adjustment::insert($temp_arr);
		}

		// dd($this->model);
		// $this->model->save();

		Alert::success('Successfully add new Outlet');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->model 					= $this->model->find($id);
		$this->data['title'] 			= "View Product ".$this->model->product_name;
		$this->data['unit']  			= $this->unit;
		$this->data['data']  			= $this->model;
		return $this->render_view('pages.product.view');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		// dd('bulkupda');
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function export(){
		return $this->build_export_cus();
	}
}
