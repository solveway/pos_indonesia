<?php namespace digipos\Http\Controllers\Front;

use digipos\models\Postcode;
use digipos\models\Province;
use digipos\models\District;
use digipos\models\Subdistrict;
use digipos\models\City;
use digipos\models\Home;
use DB;
use Illuminate\Http\Request;

class IndexController extends ShukakuController {

	public function __construct(){
		parent::__construct();
		$this->data['header_info']	= 'Home';
		$this->menu = $this->data['path'][0];
		$this->data['menu'] 		= $this->menu;
		$this->data['path'] 		= 'cek-alamat';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index(){
		// dd('Under maintain');
		$this->data['provinsi'] = Province::orderBy('name')->get();
		// dd($this->data['provinsi']);
		return $this->render_view('pages.index');
	}

	public function cek_alamat(request $request){
		dd($request->search);	
		$this->data['data'] = Postcode::join('rgn_subdistrict', 'rgn_subdistrict.id', 'postcode.subdistrict_id')->join('rgn_district', 'rgn_district.id', 'postcode.district_id')->join('city', 'city.id', 'postcode.city_id')->join('province', 'province.id', 'postcode.province_id')->select('postcode.*', 'rgn_subdistrict.name as subdistrict_name', 'rgn_district.name as district_name', 'city.name as city_name', 'province.name as province_name')->orWhere('postcode', 'like', '%' . $request->search . '%')->orWhere('rgn_subdistrict.name', 'like', '%' . $request->search . '%')->orWhere('rgn_district.name', 'like', '%' . $request->search . '%')->orWhere('city.name', 'like', '%' . $request->search . '%')->orWhere('province.name', 'like', '%' . $request->search . '%')->limit(100)->get();
		dd($this->data['data']);
		return $this->render_view('pages.index');
	}
}
