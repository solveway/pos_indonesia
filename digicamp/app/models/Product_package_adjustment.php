<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Product_package_adjustment extends Model{
	protected $table = 'product_package_adjustment';
	protected $product = 'digipos\models\Product';

	public function product(){
        return $this->belongsTo($this->product,'product_id');
    }
}