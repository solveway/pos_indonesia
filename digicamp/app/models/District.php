<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class District extends Model{
	protected $table = 'rgn_district';
}
